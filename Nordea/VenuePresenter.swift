//
//  VenuePresenter.swift
//  Nordea
//
//  Created by Juha-Matti Jääskelä on 20/06/16.
//  Copyright © 2016 Juha-Matti Jääskelä. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation

class VenuePresenter : NSObject {
    
    weak private var view : VenueView?
    private var locationManager : CLLocationManager
    private var lastLocation : CLLocation?
    
    override init() {
        self.locationManager = CLLocationManager()
        super.init()
    }
    func attach(view : VenueView) {
        self.view = view
        self.locationManager.delegate = self
        self.locationManager.distanceFilter = kCLDistanceFilterNone
        self.locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        
        
        let authStatus = CLLocationManager.authorizationStatus()
        switch authStatus {
        case .AuthorizedAlways,
             .AuthorizedWhenInUse:
            if CLLocationManager.locationServicesEnabled() {
            self.locationManager.startUpdatingLocation()
            } else {
                self.view?.error = "Please enable location services"
            }
        case .Denied,.Restricted:
            self.view?.error = "App needs authorization to proceed. Please add it"
        case .NotDetermined:
            self.locationManager.requestWhenInUseAuthorization()
        }

//        self.locationManager.startUpdatingLocation()
    }
    
    func getVenues() {
        let ret = [VenueModel]()
        defer { view?.setVenues(ret) }
        guard let long = lastLocation?.coordinate.longitude else { return }
        guard let lati = lastLocation?.coordinate.latitude else { return }
        guard let str = view?.searchText else { return }
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
        
        //TODO, getVenues should return error, network, json etc
        VenueService().getVenues(lati, longitude: long, searchStr: str) { (array) in
            
            let sortedArray = array.sort({ (lhs, rhs) -> Bool in
                return lhs.distance.intValue < rhs.distance.intValue
            })
            
            let venueModels = sortedArray.map({ (venue) -> VenueModel in
                let distance = venue.distance.stringDistance()
                return VenueModel(title: venue.title, subtitle: "\(venue.address) \(distance)" )
            })
            self.view?.setVenues(venueModels)
            
            UIApplication.sharedApplication().networkActivityIndicatorVisible = false
        }
    }
    func detach() {
        self.view = nil
        self.locationManager.stopUpdatingLocation()
    }
    
}
extension NSNumber {
    func stringDistance() -> String {
        if self.intValue < 1000 {
            return String(self.intValue) + "m"
        } else {
            return String(format: "%.2fkm", self.floatValue / 1000)
        }
    }
}
extension VenuePresenter : CLLocationManagerDelegate {
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        self.lastLocation = locations.last
    }
    
    func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        if status == .AuthorizedWhenInUse{
            self.view?.error = nil
            self.locationManager.startUpdatingLocation()
        } else {
            self.view?.error = "Location authorization needed." + (self.lastLocation != nil ? "Using old location" : "")
        }
    }
}