//
//  VenueView.swift
//  Nordea
//
//  Created by Juha-Matti Jääskelä on 20/06/16.
//  Copyright © 2016 Juha-Matti Jääskelä. All rights reserved.
//

import Foundation
import CoreLocation

protocol VenueView : NSObjectProtocol {
    func setVenues(venues : [VenueModel])
    var error : String? {
        set
        get
    }
    var searchText : String? {
        get
    }
    
}