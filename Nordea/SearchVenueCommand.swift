//
//  SearchVenueCommand.swift
//  Nordea
//
//  Created by Juha-Matti Jääskelä on 20/06/16.
//  Copyright © 2016 Juha-Matti Jääskelä. All rights reserved.
//

import Foundation

class SearchVenueCommand {
    
    //REMEMBER TO DELETE BEFORE COMMIT
    let clientId = ""
    let clientSecret = ""
    let searchString : String
    let latitude : Double
    let longitude : Double
    init(search : String, latitude : Double, longitude : Double) {
        self.latitude = latitude
        self.longitude = longitude
        self.searchString = search
    }
    func url() -> NSURL? {
        //v parameter copied from foursquare (version support from their api's)
        var ret = "https://api.foursquare.com/v2/venues/search?client_id=\(clientId)&client_secret=\(clientSecret)&ll=\(latitude),\(longitude)&v=20130815&query=\(searchString)"
        ret = ret.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!
        return NSURL(string: ret)
    }
    func httpMethod() -> String {
        return "GET"
    }
    
    func parseResult(data : NSDictionary) -> [Venue] {
        var ret = [Venue]()
        guard let response = data.valueForKey("response") as? NSDictionary else { return ret}
        guard let venues = response.valueForKey("venues") as? NSArray else { return ret}
        
        for item in venues {
            guard let name = (item as? NSDictionary)?.valueForKey("name") as? String else { continue }
            guard let location = (item as? NSDictionary)?.valueForKey("location") as? NSDictionary else {print("JMJlocation"); continue }
            guard let distance = location.valueForKey("distance") as? NSNumber  else { print("JMJdistance"); continue }
            guard let address = location.valueForKey("address") as? String  else { print ("JMJaddress"); continue }
            
            ret.append(Venue(title: name, address: address, distance:distance))
            
            
        }
        return ret
    }
}