//
//  Venue.swift
//  Nordea
//
//  Created by Juha-Matti Jääskelä on 20/06/16.
//  Copyright © 2016 Juha-Matti Jääskelä. All rights reserved.
//

import Foundation
struct Venue {
    let title : String
    let address : String
    let distance : NSNumber
    init(title:String, address:String, distance : NSNumber) {
        self.title = title
        self.address = address
        self.distance = distance
    }
}

class VenueModel {
    let title : String
    let subtitle : String
    init(title : String, subtitle : String) {
        self.title = title
        self.subtitle = subtitle
    }
}