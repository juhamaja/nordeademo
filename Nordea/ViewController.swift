//
//  ViewController.swift
//  Nordea
//
//  Created by Juha-Matti Jääskelä on 20/06/16.
//  Copyright © 2016 Juha-Matti Jääskelä. All rights reserved.
//

import UIKit
import CoreLocation

class ViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var errorText: UILabel!

    private var venues = [VenueModel]()
    private let venuePresenter = VenuePresenter()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.dataSource = self
        self.error = nil
        
        //to avoid unnecessary sepators
        self.tableView.tableFooterView = UIView()

    }
    override func viewWillAppear(animated: Bool) {
        self.venuePresenter.attach(self)
    }
    override func viewWillDisappear(animated: Bool) {
        self.venuePresenter.detach()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func textFieldDidChange(sender: UITextField) {
        if sender.text?.isEmpty == .Some(false) {
            self.venuePresenter.getVenues()
        }
    }
}



extension ViewController : VenueView {
    func setVenues(venues: [VenueModel]) {
        dispatch_async(dispatch_get_main_queue()) {
            self.venues = venues
            self.tableView.reloadData()
        }
        
    }
    var searchText : String? {
        get { return self.searchTextField.text }
    }
    var error : String? {
        set { self.errorText.text = newValue
            self.errorText.hidden = (newValue == nil)
        }
        get { return self.errorText.text }
    }
}

extension ViewController : UITableViewDataSource {
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return venues.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCellWithIdentifier("venueCell")
        let venue = venues[indexPath.row]
        cell?.textLabel?.text = venue.title
        cell?.detailTextLabel?.text = venue.subtitle
        return cell!
    }
}
