//
//  NetworkCommander.swift
//  Nordea
//
//  Created by Juha-Matti Jääskelä on 20/06/16.
//  Copyright © 2016 Juha-Matti Jääskelä. All rights reserved.
//
import Foundation

class VenueService {
    
    func getVenues(latitude : Double, longitude : Double, searchStr : String, listener : ([Venue]) -> Void) {
        let cmd = SearchVenueCommand(search: searchStr, latitude: latitude, longitude: longitude)
        guard let url = cmd.url() else { listener([Venue]()); return }
        let request = NSMutableURLRequest(URL: url)
        request.HTTPMethod = cmd.httpMethod()
        
        let session = NSURLSession.sharedSession()
        
        let task = session.dataTaskWithRequest(request, completionHandler: { ( data: NSData?, response: NSURLResponse?, error: NSError?) -> Void in
            var venues = [Venue]()
            defer {
                dispatch_async(dispatch_get_main_queue(), {
                    listener(venues)
                })
            }
            
            //validate result
            if error != nil { print(error!); return }
            guard let data = data else { return }

            let resp = response as? NSHTTPURLResponse
            print(resp?.statusCode)
            if resp?.statusCode == 200 {
                do {
                    let jsonResult = try NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.AllowFragments)
                    venues = cmd.parseResult(jsonResult as! NSDictionary)
                } catch {
                    return
                }
                
            }
            
        });
        
        task.resume()
        
    }
    
}
