//
//  NordeaTests.swift
//  NordeaTests
//
//  Created by Juha-Matti Jääskelä on 20/06/16.
//  Copyright © 2016 Juha-Matti Jääskelä. All rights reserved.
//

import XCTest
@testable import Nordea

class NordeaTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testURLSpecialCharacters() {
        //test that special url characters are correctly converted (probably most vulnerable part of code)
        let cmd = SearchVenueCommand(search: "invalid[]@!$&'()*+,;=", latitude: 0, longitude: 0)
        XCTAssert(cmd.url() != nil)

    }
    
    func testInsaneLocation() {
        let cmd = SearchVenueCommand(search: "invalid[]@!$&'()*+,;=", latitude: -1000, longitude: 1)
        XCTAssert(cmd.url() != nil)

    }
    func testInvalidResponseFromFourSquare() {
        let dict = NSMutableDictionary()
        let cmd = SearchVenueCommand(search: "invalid[]@!$&'()*+,;=", latitude: -1000, longitude: 1)
        
        XCTAssert(cmd.parseResult(dict).count == 0)
    }
    func testInvalidResponseFromFourSquare2() {
        let dict = NSMutableDictionary()
        
        dict.setValue("response", forKeyPath: "response")
        
        let cmd = SearchVenueCommand(search: "invalid[]@!$&'()*+,;=", latitude: -1000, longitude: 1)
        
        XCTAssert(cmd.parseResult(dict).count == 0)
    }
    
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock {
            // Put the code you want to measure the time of here.
        }
    }
    
}
